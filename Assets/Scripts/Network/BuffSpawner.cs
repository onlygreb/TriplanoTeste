using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class BuffSpawner : NetworkBehaviour
{
    [SerializeField]
    private List<GameObject> buffs;

    [SerializeField]
    private float delayToSpawnBuff;

    private bool canSpawn;

    private bool isSpawning;

    [ServerCallback]
    private void Start()
    {
        canSpawn = true;
    }

    [ServerCallback]
    private void Update()
    {
        if (!isSpawning)
        {
            StartCoroutine(SpawnBuff());
        }
    }

    private IEnumerator SpawnBuff()
    {
        isSpawning = true;

        yield return new WaitForSeconds(delayToSpawnBuff);

        int currentBuffToSpawn = UnityEngine.Random.Range(0, buffs.Count);

        GameObject currentSpawnedBuff = Instantiate(buffs[currentBuffToSpawn],
            new Vector3(UnityEngine.Random.Range(-30, 30), 0.3f, UnityEngine.Random.Range(-30, 30)),
            Quaternion.identity);
        NetworkServer.Spawn(currentSpawnedBuff);

        isSpawning = false;
    }
}