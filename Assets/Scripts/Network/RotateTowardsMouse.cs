using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class RotateTowardsMouse : NetworkBehaviour
{
    [SerializeField]
    GameObject objectToRotate;

    [SerializeField]
    private LayerMask mouseLayer;

    [ClientCallback]
    private void FixedUpdate()
    {
        if (!hasAuthority) return;

        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out var hit, Mathf.Infinity, mouseLayer))
        {
            Vector3 lookTowards = new Vector3(hit.point.x, objectToRotate.transform.position.y, hit.point.z);
            CmdRotateObject(lookTowards);
        }
    }

    [Command]
    private void CmdRotateObject(Vector3 v)
    {
        RpcRotateObject(v);
    }

    [ClientRpc]
    private void RpcRotateObject(Vector3 v)
    {
        objectToRotate.transform.LookAt(v);
    }
}