using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class Projectile : NetworkBehaviour
{
    private Rigidbody rb;

    [SerializeField]
    private float bulletSpeed;

    [SerializeField]
    private int damage;

    [SerializeField]
    private GameObject posExplosionEffect;

    [ServerCallback]
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        Invoke(nameof(DestroySelf), 5);
    }

    [ServerCallback]
    private void Update()
    {
        rb.AddForce(transform.forward * bulletSpeed * Time.deltaTime, ForceMode.Impulse);
    }

    [ServerCallback]
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.TryGetComponent<NetworkIdentity>(out NetworkIdentity netIden))
        {
            if (netIden.connectionToClient == connectionToClient)
            {
                DestroySelf();
                return;
            }

            if (!other.gameObject.TryGetComponent<Health>(out Health h)) return;

            h.DealDamage(damage);
            h.gameObject.GetComponent<Player>().ShakePlayerCamera();
            GameObject g = Instantiate(posExplosionEffect, transform.position, Quaternion.identity);
            NetworkServer.Spawn(g);

            DestroySelf();
            return;
        }
        else
        {
            GameObject g = Instantiate(posExplosionEffect, transform.position, Quaternion.identity);
            NetworkServer.Spawn(g);

            DestroySelf();

            return;
        }
    }


    [Server]
    public void AddIncreasedDamage(int increasedDamage)
    {
        damage += increasedDamage;
    }

    [Server]
    private void DestroySelf()
    {
        NetworkServer.Destroy(this.gameObject);
    }
}