using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class IncreasedDamageTextHandler : MonoBehaviour
{
    public static IncreasedDamageTextHandler instance;

    [SerializeField]
    private TMP_Text buffText;

    private void Awake()
    {
        instance = this;
    }

    public void ShowHideText(bool visible)
    {
        buffText.gameObject.SetActive(visible);
    }

    public void UpdateText(string text)
    {
        buffText.text = text;
    }

}
