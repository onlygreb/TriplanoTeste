using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buff : MonoBehaviour
{
    public BuffType type;

    public int damageBuff, increasedDamageTimer, healBuff;
}

public enum BuffType
{
    Heal,
    IncreasedDamage
}
