using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MenuHandler : MonoBehaviour
{
    [SerializeField]
    private TMP_InputField ipField;

    [SerializeField]
    private TMP_Text[] playerNamesText;

    [SerializeField]
    private Button startButton;

    public void HostGame()
    {
        NetworkManager.singleton.StartHost();
    }

    private void Start()
    {
        TankNetworkManager.ServerHasAddedPlayer += UpdatePlayerNamesText;
        TankNetworkManager.ServerHasRemovedPlayer += UpdatePlayerNamesText;
    }

    private void OnDestroy()
    {
        TankNetworkManager.ServerHasAddedPlayer -= UpdatePlayerNamesText;
        TankNetworkManager.ServerHasRemovedPlayer -= UpdatePlayerNamesText;
    }

    public void JoinLobby()
    {
        NetworkManager.singleton.networkAddress = ipField.text;
        NetworkManager.singleton.StartClient();
        UpdatePlayerNamesText(((TankNetworkManager) NetworkManager.singleton).GetPlayers());
    }

    private void UpdatePlayerNamesText(List<Player> players)
    {
        startButton.interactable = players.Count > 1;

        for (int i = 0; i < playerNamesText.Length; i++)
        {
            if (players.Count - 1 >= i)
            {
                playerNamesText[i].text = players[i].GetPlayerName();
            }
            else
            {
                playerNamesText[i].text = "Waiting for players...";
            }
        }
    }

    public void StartGame()
    {
        ((TankNetworkManager) NetworkManager.singleton).GetPlayers()[0].CmdStartGame();
    }
}