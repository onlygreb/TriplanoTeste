using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

public class Health : NetworkBehaviour
{
    [SyncVar(hook = nameof(HandleOnDealDamage))]
    private int currentLife;

    [SerializeField]
    private int maxLife;

    [SerializeField]
    private AudioClip postDeathSfx;

    [SerializeField]
    private GameObject sFXPlayer;

    public event Action<int, int> OnPlayerRecieveDamage;
    public static event Action<int, int> OnPlayerWithAuthorityRecieveDamage;

    [ServerCallback]
    private void Start()
    {
        currentLife = maxLife;
    }

    [Server]
    public void DealDamage(int damage)
    {
        currentLife = Mathf.Max(currentLife - damage, 0);

        if (currentLife <= 0)
        {
            GameObject sfxP = Instantiate(sFXPlayer, transform.position, quaternion.identity);
            NetworkServer.Spawn(sfxP, connectionToClient);
            RpcPlayPosDeathSFX(sfxP);

            Player p = GetComponent<Player>();
            p.RPCDisablePlayer();
            CountDownGameOverHandler.instance.RemovePlayer(p);
        }
    }

    public int GetHealth()
    {
        return currentLife;
    }

    [ClientRpc]
    private void RpcPlayPosDeathSFX(GameObject sfxP)
    {
        sfxP.GetComponent<AudioSource>().clip = postDeathSfx;
        sfxP.GetComponent<AudioSource>().Play();
    }

    [Server]
    public void Heal(int ammount)
    {
        currentLife = Mathf.Min(currentLife + ammount, maxLife);
    }

    private void HandleOnDealDamage(int oldLife, int newLife)
    {
        if (!hasAuthority)
        {
            OnPlayerRecieveDamage?.Invoke(newLife, maxLife);
        }
        else
        {
            OnPlayerWithAuthorityRecieveDamage?.Invoke(newLife, maxLife);
        }
    }
}