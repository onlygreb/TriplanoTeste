using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using TMPro;
using UnityEngine;

public class CountDownGameOverHandler : NetworkBehaviour
{
    [SerializeField]
    private TMP_Text countDownTimer, winText;

    [SerializeField]
    private GameObject[] playerHud;

    [SerializeField]
    private UpdateLifeBar uLifeBarSript;

    [SerializeField]
    private List<Player> players;


    [SerializeField, SyncVar(hook = nameof(HandleONCountDownTimerChange))]
    private int countDownSeconds = 300;

    public static CountDownGameOverHandler instance;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        if (isClient)
        {
            UpdateCountDownTimer();
        }

        if (isServer)
        {
            StartCoroutine(CountDown());
            ((TankNetworkManager) NetworkManager.singleton).SetUpPlayers();
            players = ((TankNetworkManager) NetworkManager.singleton).GetPlayers();


            foreach (Player p in players)
            {
                p.RPCEnablePlayer();
            }
        }
    }

    [ClientRpc]
    public void RPCShowWinnerText(string winner)
    {
        winText.gameObject.SetActive(true);

        winText.text = $"{winner}\nHas Won!";
    }

    [Server]
    private IEnumerator CountDown()
    {
        while (countDownSeconds > 0)
        {
            yield return new WaitForSeconds(1);
            countDownSeconds--;
        }

        EndGame();
    }

    [Client]
    private void UpdateCountDownTimer()
    {
        countDownTimer.text = ConvertSecondsToStringDataTime(countDownSeconds);
    }

    private string ConvertSecondsToStringDataTime(int seconds)
    {
        int min = 0;
        int sec = 0;

        if (seconds > 59)
        {
            min = (int) seconds / 60;
        }

        sec = seconds - min * 60;

        string result = "";

        if (sec > 9)
        {
            result = $"{min}:{sec}";
        }
        else
        {
            result = $"{min}:0{sec}";
        }

        return result;
    }

    private void HandleONCountDownTimerChange(int oldC, int newC)
    {
        UpdateCountDownTimer();
    }

    [Server]
    public void EndGame()
    {
        if (players.Count > 1)
        {
            Player highLifePlayer = null;

            foreach (Player p in players)
            {
                if (highLifePlayer == null) highLifePlayer = p;

                if (highLifePlayer.GetComponent<Health>().GetHealth() > p.GetComponent<Health>().GetHealth())
                {
                    players.Remove(p);
                }
                else
                {
                    highLifePlayer = p;
                }
            }

            ((TankNetworkManager) NetworkManager.singleton).SetPlayers(players);

            foreach (Player p in players)
            {
                p.RPCDisablePlayer();
            }

            if (players.Count > 1)
            {
                string winners = "";

                for (int i = 0; i < players.Count; i++)
                {
                    if (i < players.Count - 1)
                        winners += players[i].GetPlayerName() + " - ";
                    else
                        winners += players[i].GetPlayerName();
                }

                RPCShowWinnerText(winners);
            }
            else
            {
                RPCShowWinnerText(players[players.Count - 1].GetPlayerName());
            }
        }
        else
        {
            players[players.Count - 1].RPCDisablePlayer();
            RPCShowWinnerText(players[players.Count - 1].GetPlayerName());
        }
    }

    [Server]
    public void RemovePlayer(Player p)
    {
        players = ((TankNetworkManager) NetworkManager.singleton).GetPlayers();
        if (players.Contains(p)) players.Remove(p);

        ((TankNetworkManager) NetworkManager.singleton).SetPlayers(players);

        if (players.Count > 1) return;

        EndGame();
    }
}