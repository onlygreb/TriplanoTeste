using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : NetworkBehaviour
{
    [SerializeField]
    private GameObject lookAtObject, shootPoint, projectile, lifeCanvas, posShootEffect, sFXPlayer, tankMesh;

    [SerializeField]
    private List<GameObject> rearWalkingParticle, frontWalkingParticle;

    [SerializeField]
    private float playerSpeed, rotationSpeed, fireRate;

    [SerializeField]
    private AudioClip shootSFX;

    [SerializeField]
    private TMP_Text pNameText;

    private Rigidbody rb;

    private float h, v;

    private float shootTimer;

    private bool canShoot, hasAppliedIncreasedDamageBuff;

    private int increasedDamage;

    private string playerName;

    private bool isPlayerDisable;

    private void Start()
    {
        if (isServer)
        {
            rb = GetComponent<Rigidbody>();
            //RPCDisablePlayer();
            isPlayerDisable = true;
        }

        if (isClient && hasAuthority)
        {
            lifeCanvas.SetActive(false);
            hasAppliedIncreasedDamageBuff = false;

            canShoot = true;

            if(!SceneManager.GetActiveScene().name.StartsWith("Game")) return;

            CameraFollowTarget.instance.SetTarget(lookAtObject);
        }
    }

    [ServerCallback]
    private void Update()
    {
        if (isPlayerDisable) return;

        if (canShoot) return;

        shootTimer += Time.deltaTime;

        UpdateShootUIIndicatorImage((float) shootTimer / fireRate);

        if (shootTimer >= fireRate)
        {
            canShoot = true;

            shootTimer = 0f;

            UpdateShootUIIndicatorImage(1f);
        }
    }

    // public override void OnStartServer()
    // {
    //     RPCDisablePlayer();
    //     //DontDestroyOnLoad(gameObject);
    // }

    [Command]
    public void CmdStartGame()
    {
        ((TankNetworkManager) NetworkManager.singleton).StartGame();
    }

    [TargetRpc]
    private void UpdateShootUIIndicatorImage(float value)
    {
        ShootUIIndicator.instance.UpdateUIImage(value);
    }

    [ServerCallback]
    private void FixedUpdate()
    {
        HandleRearWalkParticles(v > 0);
        HandleFrontWalkParticles(v < 0);

        float speedZ = v * playerSpeed;
        rb.velocity = transform.forward * speedZ;

        Quaternion rotation = Quaternion.Euler(new Vector3(0, h * rotationSpeed * Time.fixedDeltaTime, 0));
        rb.MoveRotation(rb.rotation * rotation);
    }

    [ServerCallback]
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Buff"))
        {
            if (other.gameObject.TryGetComponent<Buff>(out Buff b))
            {
                switch (b.type)
                {
                    case BuffType.Heal:
                    {
                        gameObject.GetComponent<Health>().Heal(b.healBuff);
                        NetworkServer.Destroy(other.gameObject);
                        break;
                    }
                    case BuffType.IncreasedDamage:
                    {
                        if (hasAppliedIncreasedDamageBuff) return;

                        StartCoroutine(IncreasedDamageBuff(b.damageBuff, b.increasedDamageTimer));
                        NetworkServer.Destroy(other.gameObject);
                        break;
                    }
                    default: return;
                }
            }
        }
    }

    public string GetPlayerName()
    {
        return playerName;
    }

    public void SetPlayerName(string pName)
    {
        playerName = pName;
        RPCUpdatePlayerNameText();
    }

    [ClientRpc]
    private void RPCUpdatePlayerNameText()
    {
        pNameText.text = playerName;
    }

    [Server]
    private IEnumerator IncreasedDamageBuff(int iDamage, int time)
    {
        increasedDamage = iDamage;
        hasAppliedIncreasedDamageBuff = true;

        UpdateIncreasedDamageBuffText($"Increased Damage - {ConvertSecondsToStringDataTime(time)}", true);

        while (time > 0)
        {
            yield return new WaitForSeconds(1f);
            time--;
            UpdateIncreasedDamageBuffText($"Increased Damage - {ConvertSecondsToStringDataTime(time)}", true);
        }

        increasedDamage = 0;
        hasAppliedIncreasedDamageBuff = false;
        UpdateIncreasedDamageBuffText($"Increased Damage - 0:00", false);
    }

    [TargetRpc]
    private void UpdateIncreasedDamageBuffText(string text, bool visible)
    {
        IncreasedDamageTextHandler.instance.ShowHideText(visible);
        IncreasedDamageTextHandler.instance.UpdateText(text);
    }

    [ClientRpc]
    public void RPCDisablePlayer()
    {
        tankMesh.SetActive(false);
        GetComponent<PlayerInput>().enabled = false;
        GetComponent<RotateTowardsMouse>().enabled = false;
        GetComponent<UpdateLifeBar>().enabled = false;
        GetComponent<BoxCollider>().enabled = false;
        GetComponent<Rigidbody>().useGravity = false;
        lifeCanvas.SetActive(false);
        isPlayerDisable = true;
    }

    [ClientRpc]
    public void RPCEnablePlayer()
    {
        tankMesh.SetActive(true);
        GetComponent<PlayerInput>().enabled = true;
        GetComponent<RotateTowardsMouse>().enabled = true;
        GetComponent<Health>().enabled = true;
        GetComponent<UpdateLifeBar>().enabled = true;
        GetComponent<BoxCollider>().enabled = true;
        GetComponent<Rigidbody>().useGravity = false;
        isPlayerDisable = false;

        if (!hasAuthority)
        {
            lifeCanvas.SetActive(true);
        }
    }

    private string ConvertSecondsToStringDataTime(int seconds)
    {
        int min = 0;
        int sec = 0;

        if (seconds > 59)
        {
            min = (int) seconds / 60;
        }

        sec = seconds - min * 60;

        string result = "";

        if (sec > 9)
        {
            result = $"{min}:{sec}";
        }
        else
        {
            result = $"{min}:0{sec}";
        }

        return result;
    }

    [ClientRpc]
    private void HandleRearWalkParticles(bool show)
    {
        foreach (GameObject g in rearWalkingParticle)
        {
            g.SetActive(show);
        }
    }

    [ClientRpc]
    private void HandleFrontWalkParticles(bool show)
    {
        foreach (GameObject g in frontWalkingParticle)
        {
            g.SetActive(show);
        }
    }

    [Command]
    public void CmdSetPlayerMovement(float h, float v)
    {
        this.h = h;
        this.v = v;
    }

    [Command]
    public void CmdShoot()
    {
        if (!canShoot) return;

        GameObject projectileSpanwed =
            Instantiate(projectile, shootPoint.transform.position, shootPoint.transform.rotation);
        NetworkServer.Spawn(projectileSpanwed, connectionToClient);
        projectileSpanwed.GetComponent<Projectile>().AddIncreasedDamage(increasedDamage);

        GameObject sfxP = Instantiate(sFXPlayer, shootPoint.transform.position, Quaternion.identity);
        NetworkServer.Spawn(sfxP, connectionToClient);

        RpcPlayCannonShootSFX(sfxP);

        GameObject g = Instantiate(posShootEffect, shootPoint.transform.position, Quaternion.identity);
        NetworkServer.Spawn(g);

        canShoot = false;

        UpdateShootUIIndicatorImage(0f);

        ShakePlayerCamera();
    }

    [ClientRpc]
    private void RpcPlayCannonShootSFX(GameObject sfxP)
    {
        sfxP.GetComponent<AudioSource>().clip = shootSFX;
        sfxP.GetComponent<AudioSource>().Play();
    }

    [TargetRpc]
    public void ShakePlayerCamera()
    {
        StartCoroutine(CameraShake.instance.Shake(0.1f, 0.2f));
    }
}