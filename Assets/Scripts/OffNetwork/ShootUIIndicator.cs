using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShootUIIndicator : MonoBehaviour
{
    public static ShootUIIndicator instance;

    [SerializeField]
    private Image shootUIImage;

    private void Awake()
    {
        instance = this;
    }

    public void UpdateUIImage(float value)
    {
        if (value > 1) value = 1;
        if (value < 0) value = 0;

        shootUIImage.fillAmount = value;
    }
}