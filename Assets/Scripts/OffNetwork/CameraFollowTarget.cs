using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowTarget : MonoBehaviour
{
    public static CameraFollowTarget instance;

    private GameObject target;

    private void Awake()
    {
        instance = this;
    }

    public void SetTarget(GameObject target)
    {
        this.target = target;
    }

    private void FixedUpdate()
    {
        if (target == null) return;

        transform.position =
            new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z);
    }
}