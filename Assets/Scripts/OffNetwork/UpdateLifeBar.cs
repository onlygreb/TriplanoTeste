using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateLifeBar : MonoBehaviour
{
    [SerializeField]
    private Health h;

    [SerializeField]
    private Image lifeImg;

    [SerializeField]
    private bool needAuthority = false;

    private void Start()
    {
        if (needAuthority) Health.OnPlayerWithAuthorityRecieveDamage += UpdateLifeBarImage;
        else h.OnPlayerRecieveDamage += UpdateLifeBarImage;
    }

    private void OnDestroy()
    {
        if (needAuthority) Health.OnPlayerWithAuthorityRecieveDamage -= UpdateLifeBarImage;
        else h.OnPlayerRecieveDamage -= UpdateLifeBarImage;
    }

    private void UpdateLifeBarImage(int current, int max)
    {
        lifeImg.fillAmount = (float) current / max;
    }
}