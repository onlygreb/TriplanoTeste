using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class PlayerInput : NetworkBehaviour
{
    [SerializeField]
    private Player p;

    [ClientCallback]
    private void FixedUpdate()
    {
        if (!hasAuthority) return;

        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        p.CmdSetPlayerMovement(horizontal, vertical);
    }

    [ClientCallback]
    private void Update()
    {
        if (!hasAuthority) return;
       
        if (Input.GetMouseButtonDown(0))
        {
            p.CmdShoot();
        }
    }
}