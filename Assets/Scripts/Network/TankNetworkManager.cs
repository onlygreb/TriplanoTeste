using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;
using UnityEngine.SceneManagement;

public class TankNetworkManager : NetworkManager
{
    [SerializeField]
    private List<Player> players = new List<Player>();

    public static event Action<List<Player>> ServerHasAddedPlayer;
    public static event Action<List<Player>> ServerHasRemovedPlayer;

    public List<Player> GetPlayers()
    {
        return players;
    }

    public void SetPlayers(List<Player> ps)
    {
        players = ps;
    }

    public void StartGame()
    {
        if (players.Count < 2) return;

        players.Clear();
        ServerChangeScene("Game");
    }

    public void SetUpPlayers()
    {
        foreach (Player p in players)
        {

                p.RPCEnablePlayer();

        }
    }

    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        base.OnServerAddPlayer(conn);

        Player p = conn.identity.gameObject.GetComponent<Player>();
        p.SetPlayerName($"Player {players.Count}");
        players.Add(p);

        if(SceneManager.GetActiveScene().name.StartsWith("Game")) return;
        foreach (Player pl in players)
        {
            pl.RPCDisablePlayer();
        }

        ServerHasAddedPlayer?.Invoke(players);
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        Player player = conn.identity.GetComponent<Player>();
        players.Remove(player);

        ServerHasRemovedPlayer?.Invoke(players);

        base.OnServerDisconnect(conn);
    }

}