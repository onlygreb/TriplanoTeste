using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class NetworkDestroyAfterTime : NetworkBehaviour
{
    [SerializeField]
    private float timeToDestroy;

    private void Start()
    {
        Invoke(nameof(DestroyAfterTime),timeToDestroy);
    }

    private void DestroyAfterTime()
    {
        NetworkServer.Destroy(gameObject);
    }
}